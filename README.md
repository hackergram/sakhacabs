# Social Equity eXchange Bot

A [Xetrapal](https://gitlab.com/hackergram/xetrapal) based framework to facilitate [Social Equity eXchange](http://blog.hackergram.org/wp/blog/2018/03/19/social-equity-exchange-the-birds-and-the-bees/)

## Setup

`git clone`
`python install -r requirements.txt`

##### Start telegram bot listener
`python3 runseebot.py`

##### View admin dashboard
Static folder `dashboard` hosts admin dashboard.
Development hosts this on apache2.  
symlink `dashboard` directory to `/var/www/html/dashboard`

##### Start Dashboard API
`python3 dashboardapi.py`


##### Load seed data
`python3 tests/loaddata.py`