import unittest

import xetrapal

import os

class TestStartup(unittest.TestCase):
    def test_load_smriti(self):
        os.system("rm -rf ../testfiles/xetrapal-data")
        smritifile = "../testfiles/seebotxpal.conf"
        a = xetrapal.karma.load_xpal_smriti(smritifile)
        self.assertIsInstance(a, xetrapal.smriti.XetrapalSmriti)
        return a

    def test_xetrapal_instantiation(self):
        a = self.test_load_smriti()
        arjunsxpal = xetrapal.Xetrapal(a)
        self.assertIsInstance(arjunsxpal, xetrapal.Xetrapal)


if __name__ == '__main__':
    unittest.main()
